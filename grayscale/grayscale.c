#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "../stopwatch.h"

// RGB
#define CHANNELS 3

// holds data of loaded image
struct image {
	int width, height;
	int channels;
	unsigned char *data;
};

// structure passed to a function invoked by pthread_create(...)
struct thread_data {
	unsigned int slice, total_slices;
	struct image *input;
	unsigned char *output;
};

struct image *load_image(char *filename) {
	unsigned char *data;
	struct image *img = malloc(sizeof(struct image));
	if (img == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}

	data = stbi_load(filename, (int *) &img->width, (int *) &img->height,
			 (int *) &img->channels, CHANNELS);
	if (data == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}

	img->data = data;
	return img;
}

void write_image(char *filename, int w, int h, unsigned char *data) {
	stbi_write_bmp(filename, w, h, CHANNELS, data);
}

void free_image(struct image *img) {
	stbi_image_free(img->data);
	free(img);
}

void to_grayscale(int slice, int total_slices, struct image *img,
		  unsigned char *output) {
	unsigned int start_y, end_y, height;
	unsigned int source;
	unsigned char color;
	const unsigned char *pixel;

	start_y = (slice * img->height) / total_slices;
	end_y = ((slice + 1) * img->height) / total_slices;
	height = end_y - start_y;

	for (int y = start_y; y < start_y + height; ++y) {
		for (int x = 0; x < img->width; ++x) {
			source = y * img->width * CHANNELS + x * CHANNELS;
			pixel = img->data + source;
			color = (unsigned char) (0.21 * pixel[0] +
						 0.72 * pixel[1] +
						 0.07 * pixel[2]);
			for (int c = 0; c < CHANNELS; ++c) {
				output[source + c] = color;
			}
		}
	}
}

void *to_grayscale_thr(void *data) {
	struct thread_data *thr = data;
	to_grayscale(thr->slice, thr->total_slices, thr->input, thr->output);
	free(thr);
	return NULL;
}

int main(int argc, char **argv) {
	struct stopwatch *sw;
	struct image *img;
	unsigned char *output;
	int num_threads;
	pthread_t *threads;
	pthread_attr_t attr;

	if (argc != 4) {
		printf("Usage: %s <input file> <output file> <threads>",
		       argv[0]);
		exit(2);
	}

	// load image to memory
	img = load_image(argv[1]);
	if (img == NULL) {
		fprintf(stderr, "Unable to load image");
		exit(-1);
	}

	// allocate memory for output image data
	output = malloc(
		img->width * img->height * CHANNELS * sizeof(unsigned char));
	if (output == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}

	// initialize threads
	num_threads = atoi(argv[3]);
	threads = malloc((num_threads - 1) * sizeof(pthread_t));
	if (threads == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}
	pthread_attr_init(&attr);

	// start measuring time
	sw = stopwatch_start();

	// process the image
	for (int slice = 1; slice < num_threads; ++slice) {
		struct thread_data *t_data = malloc(sizeof(struct thread_data));
		if (t_data == NULL) {
			fprintf(stderr, "Unable to allocate memory");
			exit(-1);
		}
		t_data->slice = slice;
		t_data->total_slices = num_threads;
		t_data->input = img;
		t_data->output = output;

		if (pthread_create(&threads[slice - 1], &attr, to_grayscale_thr,
				   t_data) != 0) {
			perror("Can't create thread");
			free(threads);
			exit(-1);
		}
	}

	// utilize also this, parent thread
	to_grayscale(0, num_threads, img, output);

	// wait for all threads to end
	for (int i = 0; i < num_threads; ++i)
		pthread_join(threads[i], NULL);

	// stop measuring time
	printf("Time spent: %f ms\n", stopwatch_finish(sw));

	// save the output image
	write_image(argv[2], img->width, img->height, output);

	// clean up
	free_image(img);
	free(output);
	free(threads);

	// and we're done
	return 0;
}
