# README #


## This worked for us: ##
http://www.technical-recipes.com/2016/using-posix-threads-in-microsoft-visual-studio/


## The other ones ##
### To run environment on Visual studio programs please follow steps in video ###
https://www.youtube.com/watch?v=4GdTcqE0iqg

Download pthread library from: ftp://sourceware.org/pub/pthreads-win32/pthreads-w32-2-9-1-release.zip

1. Create project and click properties on application
2. Navigate to VC++ Directories
3. Edit Include directories: add path {path to downloaded pthread lib}\Pre built.2\include
4. Edit Library directories: add path {path to downloaded pthread lib}\Pre built.2\lib
5. Navigate to linker
6. Edit additional dependencies - add {path to downloaded pthread lib}\Pre-built.2\lib\x86\pthreadVC2.lib
7. Click apply
8. Copy Dlls from {path to downloaded pthread lib}\Pre-built.2\dll\x86 to C:\Windows\SysWOW64

Note: This may not work in visual studio older than 2013

### Or you can try this: ###
http://www.technical-recipes.com/2016/using-posix-threads-in-microsoft-visual-studio/

### Or you can try suggestions from teacher: ###
(Doesn't worked for me)

Postupujte takto

1. Stiahnite si 
ftp://sourceware.org/pub/pthreads-win32/pthreads-w32-2-9-1-release.zip

2.rozbalte napr. do  c:\pthreads

3. vytvorte projekt vo VS2013

4. Nastavte Additional include Directories (fig.1)I

5. Nastavte Additional Library Directories (fig.2)

6. Nastavte Post-Build Event (fig.3)

7. Nastavte Additional Dependencies (fig4)