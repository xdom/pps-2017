#!/bin/bash
SOURCE=$1
NEW_SOURCE=${SOURCE}.tmp.c
SIZE=$2
MAX_THREADS=$3
ITER=$4
cat ${SOURCE} | perl -pe 's/SIZE \d+/SIZE '"$SIZE"'/g' > ${NEW_SOURCE}

gcc ${NEW_SOURCE} -O3 -o output.tmp

for THREADS in `seq 1 ${MAX_THREADS}`;
do
    cmd="./output.tmp ${THREADS}"
    count=0;
    total=0;
    for i in `seq 1 ${ITER}`;
    do
        result=$(${cmd} | perl -nle 'print $1 if m{Time spent:\s+([0-9]*\.?[0-9]+) ms}')
        total=$(echo ${total}+${result} | bc)
        ((count++))
    done
    echo "Average execution time for ${SOURCE}, matrix size=${SIZE}, threads=${THREADS}, samples=${ITER}: "
    echo "scale=2; $total / $count" | bc
done

rm ${NEW_SOURCE}*
rm output.tmp