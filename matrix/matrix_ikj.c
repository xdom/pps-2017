//
// Created by Dominik Matta on 17/03/2017.
//

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "../stopwatch.h"

#define SIZE 100   // Size by SIZE matrices
#define PRINT_MATRICES 0
int num_threads;   // number of threads

int A[SIZE][SIZE], B[SIZE][SIZE], C[SIZE][SIZE];

// initialize a matrix
void init_matrix(int m[SIZE][SIZE]) {
	int i, j, val = 0;
	for (i = 0; i < SIZE; i++)
		for (j = 0; j < SIZE; j++)
			m[i][j] = val++ % 128;
}

void init_empty_matrix(int m[SIZE][SIZE]) {
	int i, j;
	for (i = 0; i < SIZE; i++)
		for (j = 0; j < SIZE; j++)
			m[i][j] = 0;
}

void print_matrix(int m[SIZE][SIZE]) {
	int i, j;
	for (i = 0; i < SIZE; i++) {
		printf("\n\t| ");
		for (j = 0; j < SIZE; j++)
			printf("%2d ", m[i][j]);
		printf("|");
	}
}

// thread function: taking "slice" as its argument
void *multiply(void *slice) {
	// retrieve the slice info
	int s = (intptr_t) slice;
	// compute from, to indexes
	// note that this 'slicing' works fine
	int from = (s * SIZE) / num_threads;
	// even if SIZE is not divisible by num_threads
	int to = ((s + 1) * SIZE) / num_threads;

	printf("computing slice %d (from row %d to %d)\n", s, from, to - 1);
	for (int i = from; i < to; i++) {
		for (int k = 0; k < SIZE; k++) {
			for (int j = 0; j < SIZE; j++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	printf("finished slice %d\n", s);
	return 0;
}

int main(int argc, char *argv[]) {
	struct stopwatch *sw;
	pthread_t *threads;
	int i;

	if (argc != 2) {
		printf("Usage: %s number_of_threads\n", argv[0]);
		exit(-1);
	}

	// initialize matrices
	init_matrix(A);
	init_matrix(B);
	init_empty_matrix(C);

	// initialize threads
	num_threads = atoi(argv[1]);
	threads = (pthread_t *) malloc((num_threads - 1) * sizeof(pthread_t));
	if (threads == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}

	// start measuring time
	sw = stopwatch_start();

	// this for loop not entered if thread number is specified as 1
	for (i = 1; i < num_threads; i++) {
		// creates each thread working on its own slice of i
		if (pthread_create(&threads[i - 1], NULL, multiply,
				   (void *) (intptr_t) i) != 0) {
			perror("Can't create thread");
			free(threads);
			exit(-1);
		}
	}

	// main thread works on slice 0
	// so everybody is busy
	// main thread does everything if thread number is specified as 1
	multiply(0);

	// main thread waiting for other thread to complete
	for (i = 1; i < num_threads; i++)
		pthread_join(threads[i], NULL);

	// stop measuring time
	printf("Time spent: %f ms\n", stopwatch_finish(sw));

	// clean up
	free(threads);

	// (optionally) print matrices
	if (PRINT_MATRICES) {
		printf("\n\n");
		print_matrix(A);
		printf("\n\n\t       * \n");
		print_matrix(B);
		printf("\n\n\t       = \n");
		print_matrix(C);
		printf("\n\n");
	}

	// and we're done
	return 0;
}
