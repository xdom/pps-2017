//
// Created by Dominik Matta on 20/03/2017.
//

#ifndef PPS_STOPWATCH_H
#define PPS_STOPWATCH_H

#include <time.h>

struct stopwatch {
	struct timespec start_time, finish_time;
};

struct stopwatch *stopwatch_start() {
	struct stopwatch *sw = malloc(sizeof(struct stopwatch));
	if (sw == NULL) {
		fprintf(stderr, "Unable to allocate memory");
		exit(-1);
	}
	clock_gettime(CLOCK_MONOTONIC, &sw->start_time);
	return sw;
}

double stopwatch_finish(struct stopwatch *sw) {
	double elapsed;
	clock_gettime(CLOCK_MONOTONIC, &sw->finish_time);
	elapsed = (sw->finish_time.tv_sec - sw->start_time.tv_sec) * 1000;
	elapsed += (sw->finish_time.tv_nsec - sw->start_time.tv_nsec)
		   / 1000000.0;
	free(sw);
	return elapsed;
}

#endif //PPS_STOPWATCH_H
